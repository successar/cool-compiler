# Test Commit

all: scanner parser tree driver


scanner:
	@echo ""
	@echo ""
	@echo "******"
	@echo "Building Scanner..."
	@echo "******"
	jflex cool.jflex

parser:
	@echo ""
	@echo ""
	@echo "******"
	@echo "Building Parser..."
	@echo "******"
	java -jar lib/beaver-cc-0.9.11.jar -T cool.grammar

tree:	
	@echo ""
	@echo ""
	@echo "******"
	@echo "Building AST..."
	@echo "******"
	javac ast/*.java -cp lib/beaver-rt-0.9.11.jar

driver: ast
	@echo ""
	@echo ""
	@echo "******"
	@echo "Building Driver..."
	@echo "******"
	javac CoolError.java CoolScanner.java Terminals.java CoolParser.java CoolDriver.java CoolChecker.java CoolCodegen.java -cp ./:lib/beaver-cc-0.9.11.jar


clean:
	rm -f *.class ast/*.class
	rm -f CoolScanner.java* CoolParser.java Terminals.java