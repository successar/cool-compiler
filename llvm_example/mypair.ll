;
;  Classes, objects, and methods in llvm
;
; In a Java-ish pseudocode: 
;
; class Single {
;     int x;
;     void incx(int delta) { this.x += delta; }
;     int getval() { return this.x; }
; }
;
; class Pair extends Single {
;      int y;
;      void incy(int delta) { this.y += delta; }
;      int getval( ) { return this.x + this.y; }
; }
;

target triple = "x86_64-apple-macosx10.7.0"   
%Any = type i32

%class_Single = type {
	%Any *,			; Superclass pointer
	%obj_Single* ( )*,	; constructor- a function pointer returns a obj_Single address
	void ( %obj_Single *, i32)*,	; void incx(this, int)
	i32 (%obj_Single*)*		; int getval(this)
}

; it has a class pointer and a value x
%obj_Single = type {
	%class_Single *, 	; pointer to its class
	i32			; x
}

; --- LAYOUT DEFINE FINIHSED ---
; --- LAYOUT DEFINE FINIHSED ---
; --- LAYOUT DEFINE FINIHSED ---

@Single = global %class_Single {	; a global %class_Single type object(class)
	%Any* null,
	%obj_Single* ()* @Single_constructor,	; find the address of constructor
	void (%obj_Single*,i32)* @Single_incx,
	i32 (%obj_Single*)* @Single_getval
}

define %obj_Single* @Single_constructor(){
	; return a memory block allocated.
	; get a chunk of memory
	%memchunk = call i8* @malloc(i64 8)
	; get a pointer to the memory, will return this pointer later
	%ret_obj = bitcast i8* %memchunk to %obj_Single*
	; fill this object with initial value
	;call void @init_obj(%obj_Single* %ret_obj)
	call void @init_obj(%obj_Single* %ret_obj,%class_Single* @Single)	;; test call wo/ pointer type
	ret %obj_Single* %ret_obj
}

define void @init_obj(%obj_Single* %this, %class_Single* %class){
	%caddr = getelementptr inbounds %obj_Single* %this, i32 0, i32 0
	store %class_Single* %class, %class_Single** %caddr
	%xaddr = getelementptr inbounds %obj_Single* %this, i32 0, i32 1
	store i32 111, i32* %xaddr
	ret void
}

define void @Single_incx(%obj_Single* %this, i32 %delta){
	; increase the value x by delta step
	; because the input is a pointer (say *x), the first element should be x[0]
	; that's why we have to arguments here, [0][1]
	%xaddr = getelementptr %obj_Single* %this, i32 0, i32 1
	%xval = load i32* %xaddr
	%sum = add i32 %xval, %delta
	; store -- value to address
	store i32 %sum, i32* %xaddr
	ret void
}

define i32 @Single_getval(%obj_Single* %this){
	%xaddr = getelementptr %obj_Single* %this, i32 0, i32 1
	%xval = load i32* %xaddr
	ret i32 %xval
}

define i32 @ll_main (i32 %delta){
	%myobj = call %obj_Single* @Single_constructor()
	%1 = getelementptr inbounds %obj_Single* %myobj, i32 0, i32 1
	
	; get value by direct access
	%xvalue = load i32* %1
	; get value by call function
	%xvalue2 = call i32 @Single_getval(%obj_Single* %myobj)

	; call function to change the value
	call void @Single_incx(%obj_Single* %myobj, i32 %delta)
	; get the value again
	%xvalue3 = call i32 @Single_getval(%obj_Single* %myobj)


	ret i32 %xvalue 
}

declare i8* @malloc(i64)
