package ast;
import beaver.Symbol;

public class CoolRelation extends CoolNode{
	public Symbol sa;

	public CoolRelation(CoolNode n1,int n)
	{
		super(n,n1);
		this.nodeType = 30;
	}

	public CoolRelation(CoolNode n1, Symbol o1, CoolNode n2,int n)
	{
		super(n,n1,n2);
		this.nodeType = 30;
		sa = o1;
	}

	public void accept(){
		this.nodeType = 30;
		printer.printStart("Relation");
		if(this.num==3){
			this.a.accept();
		} else {
			this.a.accept();
			printer.printComma();
			printer.printTerminal(sa);
			printer.printComma();
			this.b.accept();
		}
		printer.printEnd();
	}

}
