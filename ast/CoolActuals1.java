package ast;
import beaver.Symbol;

public class CoolActuals1 extends CoolNode{

	public Symbol sa;

	public CoolActuals1(CoolNode n1)
	{
		super(1,n1);
		this.nodeType = 2;
	}

	public CoolActuals1(CoolNode n1,Symbol o1, CoolNode n2)
	{
		super(2,n1,n2);
		this.sa = o1;
		this.nodeType = 2;
	}

	public void accept(){
		printer.printStart("Actuals1");
		this.nodeType = 2;
		if(num==2)
		{
			this.a.accept();
			printer.printComma();
			printer.printTerminal(this.sa);		
			printer.printComma();
			this.b.accept();
		}
		if(num==1)
		{
			this.a.accept();
		}
		printer.printEnd();

	}
}
