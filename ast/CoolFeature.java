package ast;
import beaver.Symbol;

public class CoolFeature extends CoolNode{

	public Symbol s1,s2,s3,s4,s5,s6,s7;

	public CoolFeature (Symbol o1,Symbol o2,Symbol o3,CoolNode n1, Symbol o4,Symbol o5,Symbol o6,CoolNode n2,Symbol o7)
	{
		super(1,n1,n2);
		this.nodeType = 19;
		s1=o1;
		s2=o2;
		s3=o3;
		s4=o4;
		s5=o5;
		s6=o6;
		s7=o7;
	}

	public CoolFeature (Symbol o1,Symbol o2,CoolNode n1,Symbol o3, Symbol o4,Symbol o5,CoolNode n2,Symbol o6)
	{
		super(2,n1,n2);
		this.nodeType = 19;
		s1=o1;
		s2=o2;
		s3=o3;
		s4=o4;
		s5=o5;
		s6=o6;
	}
	public CoolFeature (Symbol o1, Symbol o2, CoolNode n1, Symbol o3)
	{
		super(3,n1);
		this.nodeType = 19;
		s1=o1;
		s2=o2;
		s3=o3;
	}
	public CoolFeature (Symbol o1, CoolNode n1, Symbol o2, Symbol o3)
	{
		super(4,n1);
		this.nodeType = 19;
		s1=o1;
		s2=o2;
		s3=o3;
	}

	public CoolFeature (Symbol o1, Symbol o2, Symbol o3)
	{
		super(5);
		this.nodeType = 19;
		s1=o1;
		s2=o2;
		s3=o3;
	}


	public void accept(){
		this.nodeType = 19;
		printer.printStart("Feature");
		if(this.num==1){
			printer.printTerminal(s1);
			printer.printComma();
			printer.printTerminal(s2);
			printer.printComma();
			printer.printTerminal(s3);
			printer.printComma();
			this.a.accept();
			printer.printComma();
			printer.printTerminal(s4);
			printer.printComma();
			printer.printTerminal(s5);
			printer.printComma();
			printer.printTerminal(s6);
			printer.printComma();
			this.b.accept();
			printer.printComma();
			printer.printTerminal(s7);
		} else if(this.num ==2) {
			printer.printTerminal(s1);
			printer.printComma();
			printer.printTerminal(s2);
			printer.printComma();
			this.a.accept();
			printer.printComma();
			printer.printTerminal(s3);
			printer.printComma();
			printer.printTerminal(s4);
			printer.printComma();
			printer.printTerminal(s5);
			printer.printComma();
			this.b.accept();
			printer.printComma();
			printer.printTerminal(s6);
		} else if(this.num==3){
			printer.printTerminal(s1);
			printer.printComma();
			printer.printTerminal(s2);
			printer.printComma();
			this.a.accept();
			printer.printComma();
			printer.printTerminal(s3);
		} else if(this.num==4){
			printer.printTerminal(s1);
			printer.printComma();
			this.a.accept();
			printer.printComma();
			printer.printTerminal(s2);
			printer.printComma();
			printer.printTerminal(s3);
		} else if(this.num==5){
			printer.printTerminal(s1);
			printer.printComma();
			printer.printTerminal(s2);
			printer.printComma();
			printer.printTerminal(s3);
		}

		printer.printEnd();
	}

}
