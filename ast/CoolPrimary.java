package ast;
import beaver.Symbol;

public class CoolPrimary extends CoolNode{

	public Symbol sa,sb,sc;

	public CoolPrimary(Symbol o1, CoolNode n1, int n)
	{
		super(n,n1);
		this.nodeType = 27;
		sa = o1;
	}
	public CoolPrimary(Symbol o1, Symbol o2, CoolNode n1,int n)
	{
		super(n,n1);
		this.nodeType = 27;
		sa = o1;
		sb = o2;
	}
	public CoolPrimary(Symbol o1, Symbol o2, Symbol o3, CoolNode n1, int n)
	{
		super(n,n1);
		this.nodeType = 27;
		sa = o1;
		sb = o2;
		sc = o3;
	}
	public CoolPrimary(Symbol o1, CoolNode n1, Symbol o2, int n)
	{
		super(n,n1);
		this.nodeType = 27;
		sa = o1;
		sb = o2;
	}
	public CoolPrimary(Symbol o1, Symbol o2, int n)
	{
		super(n);
		this.nodeType = 27;
		sa = o1;
		sb = o2;
	}
	public CoolPrimary(Symbol o1, int n)
	{
		super(n);
		this.nodeType = 27;
		sa = o1;
	}
	

	public void accept(){
		this.nodeType = 27;
		printer.printStart("Primary");
		if(this.num==2){
			printer.printTerminal(sa);
			printer.printComma();
			this.a.accept();
		} else if(this.num==1){
			printer.printTerminal(sa);
			printer.printComma();
			printer.printTerminal(sb);
			printer.printComma();
			this.a.accept();
		} else if(this.num==3){
			printer.printTerminal(sa);
			printer.printComma();
			printer.printTerminal(sb);
			printer.printComma();
			printer.printTerminal(sc);
			printer.printComma();
			this.a.accept();
		} else if(this.num==4 || this.num==6){
			printer.printTerminal(sa);
			printer.printComma();
			this.a.accept();
			printer.printComma();
			printer.printTerminal(sb);
		} else if(this.num==5 || this.num==7){
			printer.printTerminal(sa);
			printer.printComma();
			printer.printTerminal(sb);
		} else if(this.num>=8 && this.num<=13){
			printer.printTerminal(sa);
		} 

		printer.printEnd();
	}

}
