package ast;
import beaver.Symbol;

public class CoolEquiv extends CoolNode{
	public Symbol sa;

	public CoolEquiv(CoolNode n1)
	{
		super(1,n1);
		this.nodeType = 17;
	}

	public CoolEquiv(CoolNode n1, Symbol o1, CoolNode n2)
	{
		super(2,n1,n2);
		this.nodeType = 17;
		sa = o1;
	}

	public void accept(){
		this.nodeType = 17;
		printer.printStart("Equiv");
		if(this.num==1){
			this.a.accept();
		} else {
			this.a.accept();
			printer.printComma();
			printer.printTerminal(sa);
			printer.printComma();
			this.b.accept();
		}
		printer.printEnd();
	}

}
