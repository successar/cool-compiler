package ast;
import beaver.Symbol;

public class CoolDot extends CoolNode{
	public Symbol sa, sb;


	public CoolDot(CoolNode n1)
	{
		super(1,n1);
		this.nodeType = 16;
	}

	public CoolDot(CoolNode n1,Symbol o1, CoolNode n2)
	{
		super(2,n1,n2);
		this.nodeType = 16;
		sa = o1;
	}


	public void accept(){
		this.nodeType = 16;
		printer.printStart("Dot");
		if(this.num==1){
			this.a.accept();
		} else {
			this.a.accept();
			printer.printComma();
			printer.printTerminal(sa);
			printer.printComma();
			this.b.accept();
		}
		printer.printEnd();
	}

}
