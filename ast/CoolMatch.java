package ast;
import beaver.Symbol;

public class CoolMatch extends CoolNode{
	public Symbol sa;

	public CoolMatch(CoolNode n1)
	{
		super(1,n1);
		this.nodeType = 24;
	}

	public CoolMatch(CoolNode n1, Symbol o1, CoolNode n2)
	{
		super(2,n1,n2);
		this.nodeType = 24;
		sa = o1;
	}

	public void accept(){
		this.nodeType = 24;
		printer.printStart("Match");
		if(this.num==1){
			this.a.accept();
		} else {
			this.a.accept();
			printer.printComma();
			printer.printTerminal(sa);
			printer.printComma();
			this.b.accept();
		}
		printer.printEnd();
	}

}
