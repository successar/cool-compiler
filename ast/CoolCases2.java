package ast;
import beaver.Symbol;

public class CoolCases2 extends CoolNode{

	public Symbol sa,sb;


	public CoolCases2(Symbol o1, CoolNode n1, Symbol o2)
	{
		super(1,n1);
		this.nodeType = 8;
		sa = o1;
		sb = o2;
	}

	public CoolCases2(Symbol o1, CoolNode n1, Symbol o2, CoolNode n2)
	{
		super(2,n1,n2);
		this.nodeType = 8;
		sa = o1;
		sb = o2;
	}


	public void accept(){
		this.nodeType = 8;
		printer.printStart("Cases2");
		if(this.num==1){
			printer.printTerminal(sa);
			printer.printComma();
			this.a.accept();
			printer.printComma();
			printer.printTerminal(sb);
		} else {
			printer.printTerminal(sa);
			printer.printComma();
			this.a.accept();
			printer.printComma();
			printer.printTerminal(sb);
			printer.printComma();
			this.b.accept();
		}

		printer.printEnd();
	}

}
