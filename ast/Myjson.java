package ast;
import java.io.FileWriter;
import java.io.IOException;
import beaver.Symbol;

public class Myjson
{

	public void printStartProgram(){
		System.out.printf("{");
	}

	public void printStart(){
		System.out.printf("\"CHILD\":");
		System.out.printf("{");
	}
	public void printStart(String s){
		System.out.printf("\"%s\":",s);
		System.out.printf("{");
	}

	public void printEnd(){
		System.out.printf("}");
	}

	public void printEndProgram(){
		System.out.printf("}");
	}
	public void printComma(){
		System.out.printf(",");
	}

	public void printTerminal(Symbol o1){
		String str;
		switch (o1.getId())
		{
			case 11: 
				// it is a STRING, need to strip out the quote
				str = ((String)o1.value).replaceAll("\"","").replaceAll("\\\\[a-z]","");
				break;
			default:
				str = (String)o1.value;
		}
		System.out.printf("\"%s\":\"%s\"",Terminals.NAMES[o1.getId()],str);
	}
}
