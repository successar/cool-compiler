package ast;
import beaver.Symbol;

public class CoolClass1 extends CoolNode{

	public Symbol sa;

	public CoolClass1(Symbol o1,CoolNode n1)
	{	
		super(1,n1);
		this.nodeType = 11;
		sa = o1;
	}



	public void accept(){
		this.nodeType = 11;
		printer.printStart("Class1");
		printer.printTerminal(sa);
			printer.printComma();
		this.a.accept();
		printer.printEnd();
	}

}
