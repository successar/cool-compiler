package ast;
import beaver.Symbol;

public class CoolFormals extends CoolNode{
	public Symbol sa,sb;
	
	public CoolFormals(Symbol o1,Symbol o2)
	{
		super(1);
		this.nodeType = 22;
		sa = o1;
		sb = o2;
	}

	public CoolFormals(Symbol o1,CoolNode n1,Symbol o2)
	{
		super(2,n1);
		this.nodeType = 22;
		sa = o1;
		sb = o2;
	}

	

	public void accept(){
		this.nodeType = 22;
		printer.printStart("Formals");
		if(num==1)
		{
			printer.printTerminal(sa);
			printer.printComma();
			printer.printTerminal(sb);
		}
		if(num==2)
		{
			printer.printTerminal(sa);
			printer.printComma();
			this.a.accept();
			printer.printComma();
			printer.printTerminal(sb);
		}
		printer.printEnd();
	}

}
