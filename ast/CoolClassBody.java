package ast;
import beaver.Symbol;

public class CoolClassBody extends CoolNode{
	public Symbol sa,sb;
	public CoolClassBody(Symbol o1,Symbol o2)
	{
		super(1);
		this.nodeType = 13;
		sa = o1;
		sb = o2;
	}

	public CoolClassBody(Symbol o1,CoolNode n1,Symbol o2)
	{
		super(2,n1);
		this.nodeType = 13;
		sa = o1;
		sb = o2;
	}

	public void accept(){
		this.nodeType = 13;
		printer.printStart("ClassBody");
		if(this.num==1){
			printer.printTerminal(sa);
			printer.printComma();
			printer.printTerminal(sb);
		}
		if(this.num==2){
			printer.printTerminal(sa);
			printer.printComma();
			this.a.accept();
			printer.printComma();
			printer.printTerminal(sb);
		}
		printer.printEnd();
	}

}
