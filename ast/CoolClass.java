package ast;

import beaver.Symbol;

public class CoolClass extends CoolNode{
	public Symbol sa,sb;

	public CoolClass(Symbol o1, Symbol o2, CoolNode n1, CoolNode n2)
	{
		super(1,n1,n2);
		sa = o1;
		sb = o2;
		this.nodeType = 10;
	}

	public CoolClass(Symbol o1, Symbol o2, CoolNode n1, CoolNode n2, CoolNode n3)
	{	

		super(2,n1,n2,n3);
		sa = o1;
		sb = o2;
		this.nodeType = 10;
	}

	public void accept(){
		printer.printStart("Class");
		if(this.num==1)
		{
			printer.printTerminal(sa);		
			printer.printComma();
			printer.printTerminal(sb);		
			printer.printComma();
			this.a.accept();
			printer.printComma();
			this.b.accept();
		}
		if(this.num==2)
		{
			printer.printTerminal(sa);		
			printer.printComma();
			printer.printTerminal(sb);		
			printer.printComma();
			this.a.accept();
			printer.printComma();
			this.b.accept();
			printer.printComma();
			this.c.accept();
		}
		printer.printEnd();
	}
}
