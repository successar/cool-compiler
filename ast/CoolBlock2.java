package ast;
import beaver.Symbol;

public class CoolBlock2 extends CoolNode{

	public Symbol sa,sb,sc,sd,se;
	public CoolBlock2(Symbol o1,Symbol o2,Symbol o3,Symbol o4,Symbol o5)
	{
		super(0);
		this.nodeType = 5;
		sa=o1;
		sb=o2;
		sc=o3;
		sd=o4;
		se=o5;
	}

	public void accept(){
		this.nodeType = 5;
		printer.printStart("Block2");
		printer.printTerminal(sa);
			printer.printComma();
		printer.printTerminal(sb);
			printer.printComma();
		printer.printTerminal(sc);
			printer.printComma();
		printer.printTerminal(sd);
			printer.printComma();
		printer.printTerminal(se);
		printer.printEnd();
	}

}
