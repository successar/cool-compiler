import java.io.*;
import java.util.*;
import beaver.Symbol;
import beaver.Scanner;
import beaver.Parser;

import ast.*;

public class CoolChecker{

	/*
	   CheckerAttribute
	   The attribute class for type checking.
	 */
	public static class CheckerAttribute{
		String id, type;
		public int reg=-1;
		public int addr_reg=-1;
		public int pos=-1;
		public String value; 	// value of the attribute
		public CoolNode node=null;

		public CheckerAttribute(String i, String t){
			this.id = i;
			this.type = t;
		}

		public CheckerAttribute(String i, String t,CoolNode n){
			this.id = i;
			this.type = t;
			this.node=n;
		}

	}

	/*
	   CheckerClass
	   The Class class for type checking.
	 */
	public static class CheckerClass{
		public CoolClass node;
		public String name;
		HashMap<String, CheckerMethod> methods = new HashMap<String, CheckerMethod>();
		HashMap<String, Stack> variableStack = new HashMap<String, Stack>();
		HashMap<String, CheckerAttribute> attributes = new HashMap<String, CheckerAttribute>();
		HashMap<String, CheckerAttribute> arguments = new HashMap<String, CheckerAttribute>();
		CheckerClass parent = null;

		int level = 1;

		public CheckerClass(CoolClass cnode){
			this.node = cnode;
			this.name = (String)node.sb.value;
			identifyMethods();
			identifyAttributes();
			identifyArguments(node.a);
		}

		private void identifyMethods(){
			CoolNode classbody;
			if(this.node.num==1){
				classbody = this.node.b;
			} else {
				classbody = this.node.c;
			}
			// empty class body
			if(classbody.num==1){
				return;
			}
			loopMethods(classbody.a);
		}

		private void loopMethods(CoolNode classbody1){
			if(classbody1.num == 1){
				addMethods(classbody1.a);
			} else {
				addMethods(classbody1.a);
				loopMethods(classbody1.b);
			}
		}

		private void addMethods(CoolNode feature){
			if( feature.num == 1 ){
				// with override
				CheckerMethod m = new CheckerMethod(1,feature);
				methods.put(m.name, m);
			} else if(feature.num == 2) {
				// without override
				CheckerMethod m = new CheckerMethod(2,feature);
				methods.put(m.name, m);
			}
		}

		private void identifyAttributes(){
			CoolClassBody1 body;
			if(this.node.num==1 && this.node.b.num==2){
				body = (CoolClassBody1)this.node.b.a;
			} else if( this.node.num==2 && this.node.c.num==2){
				body = (CoolClassBody1)this.node.c.a;
			} else {
				return;
			}
			while(body.num!=1){
				CoolFeature feature = (CoolFeature)body.a;
				if(feature.num==3 && feature.a.num==1){	// define variable and not native
//				if(feature.num==3 ){	// define variable and not native
					// add this attribute to c's attribute list.
					String id = (String)feature.s2.value;
					String type = (String)((CoolFeature2)feature.a).sb.value;
					this.attributes.put(id,new CheckerAttribute(id,type,feature));
				}
				body = (CoolClassBody1)body.b;
			}
			// now boyd.num==1, the last feature.
			CoolFeature feature = (CoolFeature)body.a;
			if(feature.num==3 && feature.a.num==1){	// define variable and not native
				// add this attribute to c's attribute list.
				String id = (String)feature.s2.value;
				String type = (String)((CoolFeature2)feature.a).sb.value;
				this.attributes.put(id,new CheckerAttribute(id,type,feature));
			}
		}

		private void identifyArguments(CoolNode formals){
			if(formals.num==1){
				// empty arguments
			} else {
				addArguments(formals.a);
			}
		}

		private void addArguments(CoolNode formals1){
			String id = (String)((CoolVarformals1)formals1).sb.value;
			String type = (String)((CoolVarformals1)formals1).sd.value;
			CheckerAttribute attr = new CheckerAttribute(id,type);
			arguments.put(id,attr);
			if(formals1.num==2){
				addArguments(formals1.a);
			}
		}

		private String getArgument(String name){
			String type = "NULL";
			if(arguments.containsKey(name))
				return arguments.get(name).type;
			CheckerClass c = this;
			while(c.parent!=null){
				c=c.parent;
				if(c.arguments.containsKey(name)){
					return c.arguments.get(name).type;
				}
			}
			return type;
		}

		public CheckerMethod getMethod(String mname){

			CheckerClass tmp = this;
			
			if(this.methods.containsKey(mname))
				return this.methods.get(mname);
			while(tmp.parent != null){
				// add parent to list
				tmp = tmp.parent;	
				if(tmp.methods.containsKey(mname))
					return tmp.methods.get(mname);
			}
			return null;

		}

	}

	/*
	   CheckerMethod
	   The method class for type checking.
	 */
	public static class CheckerMethod{
		public String name;
		public List<CheckerAttribute> arguments = new LinkedList<CheckerAttribute>();
		CoolNode root;
		boolean inherit;

		String type;
		CoolNode formals, methodBody;

		public CheckerMethod(int t, CoolNode node){
			this.root = node;
			this.formals = node.a;
			this.methodBody = node.b;
			if(t==1){
				this.inherit = true;
				this.name = (String)((CoolFeature)node).s3.value;
				this.type = (String)((CoolFeature)node).s5.value;
			} else if (t==2) {
				this.inherit = false;
				this.name = (String)((CoolFeature)node).s2.value;
				this.type = (String)((CoolFeature)node).s4.value;
			}
			identifyArguments(formals);

		}

		private void identifyArguments(CoolNode formals){
			if(formals.num==1){
				// empty arguments
			} else {
				addArguments(formals.a);
			}
		}

		private void addArguments(CoolNode formals1){
			String id = (String)((CoolFormals1)formals1).sa.value;
			String type = (String)((CoolFormals1)formals1).sc.value;
			CheckerAttribute attr = new CheckerAttribute(id,type);
			arguments.add(attr);
			if(formals1.num==2){
				addArguments(formals1.a);
			}
		}

		public boolean hasArgument(String s){

			for(int i=0;i<this.arguments.size();i++){
				if(this.arguments.get(i).id.equals(s)){
					return true;
				}
			}
			return false;
		}
		public CheckerAttribute getArgument(String s){

			for(int i=0;i<this.arguments.size();i++){
				if(this.arguments.get(i).id.equals(s)){
					return this.arguments.get(i);
				}
			}
			return null;
		}
		private void showArguments(){
			for(int i=0;i<this.arguments.size();i++){
				System.out.println("   ARG: "+this.arguments.get(i).id);
			}
		}

	}

	private CoolNode root;

	public boolean debug;
	public HashMap<String, CheckerClass> classes = new HashMap<String, CheckerClass>();
	public Stack methodStack = new Stack();
	public Stack caseStack = new Stack();

	public CoolChecker (CoolNode r){
		root = r;
	}


	/*
	   TypeChecking():
	   The main interface function for outside classes, provide function flow for all type checkings.
	 */
	public void typeChecking(){
		// 1. add classes, methods and attributes in one run. DONE.
		identifyClasses(root);

		// 2. add parents of classes. DONE.
		identifyClassParents();

		// 3. check circle. DONE.
		checkCircle();

		// DEBUG;
		//showClasses();

		// 4. identify inherit methods. DONE.
		identifyInheritMethods();

		// 5. identify inherit attributes. 
		identifyInheritAttributes();

		// 6. Final detail type checking. 
		deepTypeChecking();
	}

	private void showClasses(){
		System.out.println("START DEBUG");
		Set set = classes.entrySet(); 
		Iterator i = set.iterator(); 
		while(i.hasNext()) { 
			Map.Entry me = (Map.Entry)i.next(); 
			CheckerClass c = (CheckerClass)me.getValue();
			System.out.println(c.name);
		}

		System.out.println("END DEBUG");

	}

	// Classs related.
	private void identifyClasses(CoolNode node){
		if(node.num==1){
			addClass(node.a);
		} else if(node.num==2){
			addClass(node.a);
			identifyClasses(node.b);
		}
	}

	private void identifyClassParents(){
		Set set = classes.entrySet(); 
		Iterator i = set.iterator(); 
		while(i.hasNext()) { 
			Map.Entry me = (Map.Entry)i.next(); 
			CheckerClass c = (CheckerClass)me.getValue();
			if(c.node.num == 2) {
				// have parents
				// check if parent's name exists
				String parentName = (String)((CoolClass2)c.node.b.a).sa.value;
				if(!parentName.equals("native")){ 	// ignore native type
					if(!classes.containsKey(parentName)){
						CoolError.errorReport("Incomplete inherits!");
						// throw error
					} else {
						c.parent = classes.get(parentName);
					}
				}
			}

		}

	}


	private void checkCircle(){
		Set set = classes.entrySet(); 
		Iterator i = set.iterator(); 
		while(i.hasNext()) { 
			Map.Entry me = (Map.Entry)i.next(); 
			CheckerClass c = (CheckerClass)me.getValue();
			// do check
			ArrayList<String> parents = new ArrayList<String>();
			while(c.parent!=null){
				if(parents.indexOf(c.parent.name)!=-1){
					CoolError.errorReport("ERROR: CIRCULATION!" + c.name);
					break;
				}
				parents.add(c.parent.name);
				c=c.parent;
			}
		}
	}

	private void addClass(CoolNode cnode){
		// give a class node, add the name of the class and the node itself into HashMap
		CheckerClass c = new CheckerClass((CoolClass)cnode);
		classes.put(c.name, c);
	}

	// Methods related
	/*
	   identifyInheritMethods():
	   Check if methods confirms with super classes's methods.
	 */
	private void identifyInheritMethods(){
		Iterator i = classes.entrySet().iterator(); 
		while(i.hasNext()) { 
			Map.Entry me = (Map.Entry)i.next(); 
			CheckerClass c = (CheckerClass)me.getValue();

			// collect parent, grand parents
			List<CheckerClass> parents = new LinkedList<CheckerClass>();
			CheckerClass tmp = c;

			// *** if there is circle in classes, this line will run infinitely ***//
			while(tmp.parent != null){
				// add parent to list
				parents.add(tmp.parent);
				tmp = tmp.parent;	
			}

			// have class, check methods.
			Iterator mi = c.methods.values().iterator();
			while(mi.hasNext()){
				// for each method, check all parents.
				CheckerMethod m = (CheckerMethod)(mi.next());
				for(CheckerClass p: parents){
					if(p.methods.containsKey(m.name)){
						// if overriden
						checkMethodsArguments(p.methods.get(m.name),m);

					}
				}
			}
		}
	}

	private void checkMethodsArguments(CheckerMethod p, CheckerMethod m){
		// First check arguments number

		if(!(p.arguments.size()==m.arguments.size())){
			CoolError.errorReport("*** METHOD: Arugment size diff!");
		}
		// Check if every argument is the same type
		for(int i=0;i<m.arguments.size();i++){
			if(!m.arguments.get(i).type.equals(p.arguments.get(i).type)){
				CoolError.errorReport("METHOD: Argument type diff! "+m.arguments.get(i).id);
			}
		}
	}

	private void identifyInheritAttributes(){
		// all classes' attributes have been added to their attribute list.
		// now need to check inherit conflicts.
		// same as checking method's, just simply not existing in super class's list.

		Iterator i = classes.entrySet().iterator(); 
		while(i.hasNext()) { 
			Map.Entry me = (Map.Entry)i.next(); 
			CheckerClass c = (CheckerClass)me.getValue();

			// collect parent, grand parents
			List<CheckerClass> parents = new LinkedList<CheckerClass>();
			CheckerClass tmp = c;

			// *** if there is circle in classes, this line will run infinitely ***//
			while(tmp.parent != null){
				// add parent to list
				parents.add(tmp.parent);
				tmp = tmp.parent;	
			}

			// have class, check methods.
			Iterator mi = c.attributes.values().iterator();
			while(mi.hasNext()){
				// for each method, check all parents.
				CheckerAttribute m = (CheckerAttribute)(mi.next());
				for(CheckerClass p: parents){
					if(p.attributes.containsKey(m.id)){
						// if exists, ERROR
						CoolError.errorReport("ATTR: EXISTING ATTR in SUPER CLASS");	
					}
				}
			}
		}


	}

	private void deepTypeChecking(){

		Iterator i = classes.entrySet().iterator(); 
		while(i.hasNext()) { 
			// For each class
			Map.Entry me = (Map.Entry)i.next(); 
			CheckerClass c = (CheckerClass)me.getValue();
			checkTypes(c.node,c);
		}
	}

	/*
	   Types -- Int
	   ID 	0
	   Null	1
	   INTEGER	2
	   STRING	3
	   BOOLEAN	4
	   THIS	5
	   UNIT	6
	 */
	private void checkTypes(CoolNode n, CheckerClass c){
		CheckerClass tmp;
		switch(n.nodeType){
			case 27:	// primary
				switch(n.num){
					case 1:		// NEW TYPE
						checkChildren(n,c);	
						String t = (String)((CoolPrimary)n).sb.value;
						if(t.equals("Unit") || t.equals("Any") || t.equals("Boolean") || t.equals("Symbol") ||t.equals("Int") ){
							CoolError.errorReport("ERROR: NEW ERROR");
						}
						n.returnType = t;
						break;
					case 2:		// ID actuals
						// Function call!
						checkChildren(n,c);	
						String mname = (String)((CoolPrimary)n).sa.value;
						tmp = c;
						while(!tmp.methods.containsKey(mname)){
							if(tmp.parent == null){
								//	CoolError.errorReport("NON-EXISTING METHOD "+mname+" "+c.name);
								break;
							}
							tmp=tmp.parent;
						}
						if(tmp.methods.containsKey(mname)){
							n.returnType = tmp.methods.get(mname).type;
						} 

						break;
					case 3:		// SUPER DOT ID actuals	
						// Super class's function call!
						checkChildren(n,c);	
						if(c.parent==null){
							CoolError.errorReport("SUPER: NO PARENT CLASS!");
							break;
						}
						String m = (String)((CoolPrimary)n).sc.value;

						CheckerClass ck = c.parent;
						tmp = ck;
						while(!tmp.methods.containsKey(m)){
							if(tmp.parent == null){
								CoolError.errorReport("SUPER: NON-EXISTING METHOD "+m+" "+ck);
								break;
							}
							tmp=tmp.parent;
						}
						if(tmp.methods.containsKey(m)){
							CheckerMethod meth = tmp.methods.get(m);
							CoolActuals a = (CoolActuals)n.a;
							if(a.num==1){
								if(meth.arguments.size()!=0){
									CoolError.errorReport("SUPER: 1: USING UNMATCHED ARGUMENTS!" + meth.name+ " " +((CoolDot)n).sa.getStart());
								}
							} else {
								int i = 0;
								CoolActuals1 a1 = (CoolActuals1)a.a;
								while(a1.num!=1){
									if(!a1.a.returnType.equals(meth.arguments.get(i).type)){
										CoolError.errorReport("DOT: 2: USING UNMATCHED ARGUMENTS!" + meth.name+ " " +((CoolDot)n).sa.getStart());
										break;
									}

									a1 = (CoolActuals1)a1.b;
									i++;
								}
								//if(!a1.a.returnType.equals(meth.arguments.get(i).type)){
								if(!isSubType(a1.a.returnType,meth.arguments.get(i).type)){
									CoolError.errorReport("SUPER: 3: USING UNMATCHED ARGUMENTS!" + meth.name+ " " +((CoolDot)n).sa.getStart());
									break;
								}

								}
							n.returnType = tmp.methods.get(m).type;
							} else {
								CoolError.errorReport("SUPER: NON-EXISTING METHOD "+m+" "+c.name);
								n.returnType = "ERROR";
							}

							break;
							case 4:		// {block}
							c.level++;
							checkChildren(n,c);	
							c.level--;
							n.returnType = n.a.returnType;
							break;
							case 5:		// {}
							checkChildren(n,c);	
							n.returnType = "Unit";
							break;
							case 6:		// (expr)
							checkChildren(n,c);	
							n.returnType = n.a.returnType;
							break;
							case 7:		// ()
							checkChildren(n,c);	
							n.returnType = "Unit";
							break;
							case 8:		// NULL
							checkChildren(n,c);	
							n.returnType = "Null";
							break;
							case 9:		// ID
							checkChildren(n,c);	
							//n.returnType = "Id";
							// the variable could be defined in:
							// 	class attributes
							//	method arguments
							//	local variable
							//	class arguments
							//	cases arguments
							String name = (String)((CoolPrimary)n).sa.value;
							n.returnType = getIdType(name,c);
							break;
							case 10:	// INTEGER	
							checkChildren(n,c);	
							n.returnType = "Int";
							break;
							case 11:	// STRING	
							checkChildren(n,c);	
							n.returnType = "String";
							break;
							case 12:	// BOOLEAN	
							checkChildren(n,c);	
							n.returnType = "Boolean";
							break;
							case 13:	// THIS	
							checkChildren(n,c);	
							n.returnType = c.name;
							break;
						}
						break;
					case 0:
						//				checkChildren(n,c);	
						CoolError.errorReport("WWWWRRRROOOONNNNGGGG NODETYPE!");
						break;
					case 1:		// actuals
						checkChildren(n,c);	
						break;
					case 2:		// actuals1
						checkChildren(n,c);	
						break;
					case 3:		// block
						checkChildren(n,c);	
						break;
					case 4:		// block1
						checkChildren(n,c);	
						break;

					case 5:		// block2
						checkChildren(n,c);	
						// Define local variable. Maintain variableStack.
						String id = (String)((CoolBlock2)n).sb.value;
						String type = (String)((CoolBlock2)n).sd.value;
						if(c.variableStack.containsKey(id)){
							// have already defined this variable
							Stack s = c.variableStack.get(id);
							while(s.size()>=c.level){
								s.pop();
							}
							s.push(type);
						} else {
							Stack s = new Stack();
							s.push(type);
							c.variableStack.put(id,s);
						}
						break;

					case 6:		// cases
						c.level++;
						checkChildren(n,c);	
						c.level--;
						n.returnType = n.a.returnType;
						break;
					case 7:		// cases1
						checkChildren(n,c);	
						n.returnType = n.a.returnType;
						break;
					case 8:		// cases2
						checkChildren(n,c);	
						if(caseStack.size()!=0){
							caseStack.pop();
						}
						n.returnType = n.a.returnType;
						break;
					case 9:		// cases3
						checkChildren(n,c);	
						if(n.num == 1)
						{
							caseStack.push(new CheckerAttribute((String)((CoolCases3)n).sa.value,(String)((CoolCases3)n).sc.value));
							n.returnType = (String)((CoolCases3)n).sc.value;
						} else {
							n.returnType = "CaseNull";
						}

						break;
					case 10:	// class
						checkChildren(n,c);	
						break;
					case 11:	// class1
						checkChildren(n,c);	
						break;
					case 12:	// class2
						checkChildren(n,c);	
						break;
					case 13:	// classbody
						checkChildren(n,c);	
						break;
					case 14:	// classbody1
						checkChildren(n,c);	
						break;

					case 15:	// control
						checkChildren(n,c);	
						if(n.num==1){	// if
							if(!n.a.returnType.equals("Boolean")){
								CoolError.errorReport("DEEPCHECK: IF ERROR :" + n.a.returnType);
							}
							// return type should be b or c's return type
							if(isSubType(n.b.returnType, n.c.returnType))
								n.returnType = n.c.returnType;
							else
								n.returnType = n.b.returnType;
						} else if (n.num==2) { 		// while(expr)
							if(!n.a.returnType.equals("Boolean")){
								CoolError.errorReport("DEEPCHECK: WHILE ERROR ::" + n.a.returnType);
							}
							n.returnType = "Unit";
						} else {
							n.returnType = n.a.returnType;
						}
						break;

					case 16:	// dot
						checkChildren(n,c);	
						if(n.num==1){	// primary
							n.returnType = n.a.returnType;
						} else {
							// dot DOT primary
							String t = n.a.returnType;
							String m = (String)((CoolPrimary)n.b).sa.value;

							// SHOULD GET THE CORRECT RETURN VALUE FIRST!
							if(!classes.containsKey(t)){
//								CoolError.errorReport(""+caseStack.size());
								CoolError.errorReport("NON-EXISTING RETURN TYPE at Line "+((CoolDot)n).sa.getStart()+ " "+m+" "+t);
								break;
							}
							CheckerClass ck = classes.get(t);	
							tmp = ck;
							while(!tmp.methods.containsKey(m)){
								if(tmp.parent == null){
									CoolError.errorReport("NON-EXISTING METHOD "+m+" "+ck);
									break;
								}
								tmp=tmp.parent;
							}
							if(tmp.methods.containsKey(m)){
								CheckerMethod meth = tmp.methods.get(m);
								CoolActuals a = (CoolActuals)n.b.a;
								if(a.num==1){
									if(meth.arguments.size()!=0){
										CoolError.errorReport("DOT: 1: USING UNMATCHED ARGUMENTS!" + meth.name+ " " +((CoolDot)n).sa.getStart());
									}
								} else {
									int i = 0;
									CoolActuals1 a1 = (CoolActuals1)a.a;
									while(a1.num!=1){
										if(!a1.a.returnType.equals(meth.arguments.get(i).type)){
											CoolError.errorReport("DOT: 2: USING UNMATCHED ARGUMENTS!" + meth.name+ " " +((CoolDot)n).sa.getStart());
											break;
										}

										a1 = (CoolActuals1)a1.b;
										i++;
									}
									//if(!a1.a.returnType.equals(meth.arguments.get(i).type)){
									if(!isSubType(a1.a.returnType,meth.arguments.get(i).type)){
										CoolError.errorReport("DOT: 3: USING UNMATCHED ARGUMENTS!" + meth.name+ " " +((CoolDot)n).sa.getStart());
										break;
									}

									}
								n.returnType = tmp.methods.get(m).type;
								} else {
									CoolError.errorReport("DOT: NON-EXISTING METHOD "+m+" "+c.name);
									n.returnType = "ERROR";
								}

							}
							break;

							case 17:	// equiv
							checkChildren(n,c);	
							if(n.num==1){
								n.returnType = n.a.returnType;
							} else {
								// a == b
								if(!n.a.returnType.equals(n.b.returnType)){
									CoolError.errorReport("DOT: DEEPCHECK: EQUIV ERROR "+((CoolEquiv)n).sa.getStart()+" ->" + n.a.returnType + " " + n.b.returnType);
								} else {
									//						CoolError.errorReport("EQUIV Correct " + n.a.returnType);
								}
								n.returnType = "Boolean";
							}
							break;

							case 18:	// expr
							checkChildren(n,c);	
							if(n.num==1){
								// ID = expr
								// Check 
								String name = (String)((CoolExpr)n).sa.value;
								if(!isSubType(n.a.returnType, getIdType(name,c))){
									CoolError.errorReport("EXPR: ASSIGNMENT TYPE ERROR!" + getIdType(name,c)+ " = " +n.a.returnType);
									break;
								}
							} else {
								n.returnType = n.a.returnType;
							}

							break;


							case 19:	// feature
							// if it is a define of methods, push arguments into a stack.
							if(n.num<=2){
								String name= new String();
								if(n.num==1){
									name = (String)((CoolFeature)n).s3.value;
								} else if (n.num==2) {
									name = (String)((CoolFeature)n).s2.value;
								}

								methodStack.push(c.methods.get(name));
								checkChildren(n,c);	
								methodStack.pop();

							} else {
								checkChildren(n,c);	
							}
							break;

							case 20:	// feature1
							checkChildren(n,c);	
							break;

							case 21: 	// feature2
							checkChildren(n,c);	
							break;
							case 22:	// formals
							checkChildren(n,c);	
							break;
							case 23:	// formals2
							checkChildren(n,c);	
							break;
							case 24:	// match
							checkChildren(n,c);	
							if(n.num==1){
								n.returnType = n.a.returnType;
							} else {
								//n.returnType = "Null"; 	// NULL
								if(!(isSubType(n.a.returnType,n.b.returnType) ||isSubType(n.b.returnType,n.a.returnType))){
									CoolError.errorReport("MATCH: ID TYPE NOT FIT! "+n.a.returnType+" "+n.b.returnType);
								} else {
									// TODO: union type of all cases' return value

								}
							}
							break;
							case 25:	// neg
							checkChildren(n,c);	
							if(n.num == 2){
								// NOT
								if(!(n.a.returnType.equals("Boolean"))){
									CoolError.errorReport("NEG: DEEPCHECK: NEG ERROR " + n.a.returnType);
								}
							} else if (n.num==1){
								// MINUS
								if(!(n.a.returnType.equals("Int"))){
									CoolError.errorReport("NEG: DEEPCHECK: MINUS ERROR " + n.a.returnType);
								}
							}
							n.returnType = n.a.returnType;

							break;
							case 29:	// program
							checkChildren(n,c);	
							break;
							case 30:	// relation
							checkChildren(n,c);	
							if(n.num==1 || n.num==2){	// relation
								if(!(n.a.returnType.equals(n.b.returnType) && n.a.returnType.equals("Int"))){
									//ERROR
									CoolError.errorReport("RELATION: ERROR "+((CoolRelation)n).sa.getStart()+" -> " + n.a.returnType + " " + n.b.returnType);
								}
								n.returnType = "Boolean";
							} else{
								n.returnType = n.a.returnType;
							}
							break;

							case 28:	// prod
							checkChildren(n,c);	
							if(n.num==1 || n.num==2){	// PLUS
								if(!(n.a.returnType.equals(n.b.returnType) && n.a.returnType.equals("Int"))){
									CoolError.errorReport("PROD: PROD ERROR "+((CoolProd)n).sa.getStart() +" -> "+n.a.returnType+ " : "+ n.b.returnType);
								}
								else {
								}
							}
							n.returnType = n.a.returnType;
							break;
							case 31:	// sum
							checkChildren(n,c);	
							if(n.num==1 || n.num==2){	// PLUS
								if(!(n.a.returnType.equals(n.b.returnType) && n.a.returnType.equals("Int"))){
									CoolError.errorReport("SUM: SUM ERROR "+((CoolSum)n).sa.getStart() +" -> "+n.a.returnType+ " : "+ n.b.returnType);
								}
								else {
								}
							}
							n.returnType = n.a.returnType;
							break;
							case 32:	// varformals
							checkChildren(n,c);	
							break;
							case 33:	// varformals1
							checkChildren(n,c);	
							break;
							default:
							break;
						}

				}

				// Check if Type a is a subType of Type b
				private boolean isSubType(String a, String b){
					if(a.equals(b) || b.equals("Any"))
						return true;
					if(!classes.containsKey(a)){
						return false;
					}
					CheckerClass c = classes.get(a);
					while(c.parent!=null){
						if(c.parent.name.equals(b)){
							return true;
						}
						c=c.parent;
					}
					return false;
				}
				private String getIdType(String name, CheckerClass c){

					if(c.variableStack.containsKey(name)){
						return (String)c.variableStack.get(name).peek();
					} else if(this.methodStack.size()>0 &&((CheckerMethod)this.methodStack.peek()).hasArgument(name) ) {
						return ((CheckerMethod)this.methodStack.peek()).getArgument(name).type;
					} else if (c.attributes.containsKey(name)){
						return c.attributes.get(name).type;
					} else if (this.caseStack.size()>0 && ((CheckerAttribute)this.caseStack.peek()).id.equals(name)){
						return ((CheckerAttribute)this.caseStack.peek()).type;
					} else if (!c.getArgument(name).equals("NULL")){
						return c.getArgument(name);
					} else {
						CoolError.errorReport("WRONG TYPE"+name+" "+c.arguments.size());
						return "NULL";
					}
				}

				private void checkChildren(CoolNode n, CheckerClass c){
					if(n.a!=null)
						checkTypes(n.a,c);
					if(n.b!=null)
						checkTypes(n.b,c);
					if(n.c!=null)
						checkTypes(n.c,c);
				}

		}
