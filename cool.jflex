//package cool;
import beaver.Symbol;
import beaver.Scanner;

/* 
	Terminals.java has all symbols.
*/

%%

%class CoolScanner
%extends Scanner
%function nextToken
%type Symbol
%yylexthrow Scanner.Exception
%line
%column
%eofval{
	return new Symbol(Terminals.EOF, "end-of-file");
%eofval}
	
%{

/**
 * Return a new Symbol with the given token id, and with the current line and
 * column numbers.
 */
Symbol newSym(short tokenId) {
    return new Symbol(tokenId, yyline, yycolumn);
}

/**
 * Return a new Symbol with the given token id, the current line and column
 * numbers, and the given token value.  The value is used for tokens such as
 * identifiers and numbers.
 */
Symbol newSym(short tokenId, Object value) {
    return new Symbol(tokenId, yyline, yycolumn, value);
}

%}


/*-*
 * PATTERN DEFINITIONS:
 *	integer, type/object id, special notation, strings, comments,
 *	keywords, white space
 */

/* integer */
digit		= [0-9]
nonzero_digit	= [1-9]
nonzero_integer	= {nonzero_digit}{digit}*
integer		= [0]|{nonzero_integer}

/* type/object id */
letter		= [A-Za-z]
cap_letter	= [A-Z]
low_letter	= [a-z]
other_id_char	= [_]
type_id		= {cap_letter}({digit}|{letter}|{other_id_char})*
object_id	= {low_letter}({digit}|{letter}|{other_id_char})*
//id		= {type_id}|{object_id}

/* string: simple and long */
escape		= \\[0btnrf\"\\]
shortchar	= [^\\\n\"]
shortitem	= {shortchar}|{escape}
longchar	= [.\n\r]
longitem	= {longchar}
shortstring	= \"{shortitem}*\"
longstring	= \"\"\"(.|[\n\r])*\"\"\"
//longstring	= \"\"\"[.]*\"\"\"
//longstring	= \"\"\"{longitem}*\"\"\"
string		= {shortstring}|{longstring}

/* comments */
linecomment	= \/\/[^\n]*\n
starcomment	= \/\*([^*]|[\r\n]|(\*+([^*/]|[\r\n])))*\*+\/
comment		= {linecomment}|{starcomment}

/* special notations */
/*
leftbrace	= \{
rightbrace	= \}
slash		= \/
backslash	= \\
star		= \*
doublequote	= \"
*/

/* white space */
whitespace	= [\t\n\r ]

/* reserved keywords */
reserved	= (abstract|catch|do|final|finally|for|forSome|implicit|import|lazy|object|package|private|protected|requires|return|sealed|throw|trait|try|type|val|with|yield)

%%
/**
 * LEXICAL RULES:
 */

/* reserved keywords */
{reserved}	{ CoolError.errorReport("Reserved Keywords",yytext());return newSym(Terminals.ID, yytext());}

{comment}	{} /* do nothing on comment */
"/*"		{ CoolError.errorReport("Unfinished comment",yytext());return newSym(Terminals.ID, yytext());}
"("		{ return newSym(Terminals.LPAREN, yytext()); } 
"{"		{ return newSym(Terminals.LBRACE, yytext()); } 
"\0"		{ return newSym(Terminals.NULL, yytext()); } 
"-"		{ return newSym(Terminals.MINUS, yytext()); } 
"!"		{ return newSym(Terminals.NOT, yytext()); } 
")"		{ return newSym(Terminals.RPAREN, yytext()); } 
":"		{ return newSym(Terminals.COLON, yytext()); } 
"<="		{ return newSym(Terminals.LE, yytext()); } 
"<"		{ return newSym(Terminals.LT, yytext()); } 
"=="		{ return newSym(Terminals.EQUALS, yytext()); } 
"*"		{ return newSym(Terminals.TIMES, yytext()); } 
"/"		{ return newSym(Terminals.DIV, yytext()); } 
"+"		{ return newSym(Terminals.PLUS, yytext()); } 
"}"		{ return newSym(Terminals.RBRACE, yytext()); } 
";"		{ return newSym(Terminals.SEMI, yytext()); } 
","		{ return newSym(Terminals.COMMA, yytext()); } 
"=>"		{ return newSym(Terminals.ARROW, yytext()); } 
"."		{ return newSym(Terminals.DOT, yytext()); } 
"="		{ return newSym(Terminals.ASSIGN, yytext()); } 
case		{ return newSym(Terminals.CASE, yytext()); } 
super		{ return newSym(Terminals.SUPER, yytext()); } 
new		{ return newSym(Terminals.NEW, yytext()); } 
//true		{ return newSym(Terminals.BOOLEAN, new Boolean(yytext())); } 
//false		{ return newSym(Terminals.BOOLEAN, new Boolean(yytext())); } 
true		{ return newSym(Terminals.BOOLEAN, new String("True")); } 
false		{ return newSym(Terminals.BOOLEAN, new String("False")); } 
this		{ return newSym(Terminals.THIS, yytext()); } 
if		{ return newSym(Terminals.IF, yytext()); } 
while		{ return newSym(Terminals.WHILE, yytext()); } 
var		{ return newSym(Terminals.VAR, yytext()); } 
def		{ return newSym(Terminals.DEF, yytext()); } 
native		{ return newSym(Terminals.NATIVE, yytext()); } 
class		{ return newSym(Terminals.CLASS, yytext()); } 
else		{ return newSym(Terminals.ELSE, yytext()); } 
extends		{ return newSym(Terminals.EXTENDS, yytext()); } 
override	{ return newSym(Terminals.OVERRIDE, yytext()); } 
match		{ return newSym(Terminals.MATCH, yytext()); } 
null		{ return newSym(Terminals.NULL, yytext()); } 



//{integer}	{ return newSym(Terminals.INTEGER, new Integer(yytext())); } 
{integer}	{ return newSym(Terminals.INTEGER, yytext()); } 
{object_id}	{ return newSym(Terminals.ID, yytext()); } 
{type_id}	{ return newSym(Terminals.TYPE, yytext()); } 
{string}	{ return newSym(Terminals.STRING, yytext()); } 
{whitespace}	{} /* IMPORTANT  TO HAVE THIS! do nothing on comment */


//.|\n		{ throw new Error("Illegal character < "+ yytext()+" >"); }
.|\n		{ CoolError.errorReport("Illegal Character",yytext()); }
