//package cool;

import java.io.*;
import java.util.*;
import beaver.Symbol;
import beaver.Scanner;
import beaver.Parser;

import ast.*;

public class CoolCodegen{
	CoolChecker checker;
	int outputType;
	FileWriter fstream;
	BufferedWriter out;
	int globalid=1;
	int globallabel=1;
	int globalendlabel=1;
	int indent=1;

	HashMap<String, Integer> objVarPosition = new HashMap<String,Integer>();
	int varpos = 1;

	int lastreg=-1;
	int dotreg=-2;
	String dotString="";


	// constructor, take a done checker to take advantage of the work finished in type checking phase.
	public CoolCodegen(CoolChecker c, int t){
		this.checker = c;
		this.outputType = t;
		// create output file 
		if(t == 1){
			try{
				fstream = new FileWriter("cool.ll");
				out = new BufferedWriter(fstream);
			}catch (Exception e){//catch exception if any
				System.err.println("file creating error: " + e.getMessage());
			}
		}
	}

	public void generateCode(){

		// 0. generate initial code for llvm
		genInit();

		// 1. generate class definitions
		// done
		genClassesDef();

		// 2. generate methods definitions
		// TODO: method definitions
		//		genmethodsdef();

		// 3. generate methods implementations
		//		genmethodsimp();
		genEnd();

	}

	private void genInit(){
		output("target triple = \"x86_64-apple-macosx10.7.0\"");
		output("");
		output("%class_Int = type i32  ");
		output("%class_Boolean = type i32");
		output("%class_Unit = type i32 ");
		output("%class_String = type i32 ");
		output("%class_native = type i32 ");
		output("");
	}
	private void genEnd(){
		output("");
		output("declare i8* @malloc(i64)");
	}

	private void genClassesDef(){
		Set set = this.checker.classes.entrySet();
		Iterator i = set.iterator();
		while(i.hasNext()) {
			Map.Entry me = (Map.Entry)i.next();
			CoolChecker.CheckerClass c = (CoolChecker.CheckerClass)me.getValue();

			String classname = ((String)((CoolClass)c.node).sb.value);

			// class definition
			output("%class_"+classname+" = type {");
			if(c.parent!=null){
				output("%class_"+c.parent.name+" *,",1);
			} else {
				output("%class_Any *,",1);
			}
				//constructor
			output("%object_"+classname+"* ( )*,",1);
			genClassMethods(c,1);
			output("}");

			// class allocation
			output("@"+classname+" = global %class_"+classname+" {");
			if(c.parent!=null){
				output("%class_"+c.parent.name+"* @"+c.parent.name+",",1);
			} else {
				output("%class_Any* null,",1);
			}
			output("%object_"+classname+"* ( )* @"+classname+"_constructor,",1);
			genClassMethods(c,2);
			output("}");

			// object type definition
			output("%object_"+classname+" = type {");
			int size=c.attributes.size() + c.arguments.size();
			if(size==0)
				output("%class_"+classname+" *",1);
			else
				output("%class_"+classname+" *,",1);
			genClassArguments(c,1);
			genClassAttributes(c,1);
			varpos=1;
			output("}");


			genClassConstructor(c);
			genClassMethods(c,3);
			output("");
		}
	}
	
	private void genClassConstructor(CoolChecker.CheckerClass c){
		output("define %object_"+c.name+"* @"+c.name+"_constructor(){");
		output("%memchunk = call i8* @malloc(i64 8)",1);
		output("%ret_obj = bitcast i8* %memchunk to %object_"+c.name+"*",1);
		genClassArguments(c,3);
		genClassAttributes(c,3);
		output("ret %object_"+c.name+"* %ret_obj",1);
		output("}");
		globalid=1;
	}

	private void genClassArguments(CoolChecker.CheckerClass c, int t){
		Iterator ai = c.arguments.values().iterator();
		while(ai.hasNext()){
			CoolChecker.CheckerAttribute attr = (CoolChecker.CheckerAttribute)(ai.next());
			attr.pos=varpos;
			varpos++;
			if(ai.hasNext() || c.attributes.size()!=0){
				if(t==1){
					output("%class_"+attr.type+" ," ,1);
				} else if (t==2) {
					output("%class_"+attr.type+" "+attr.id+"," ,1);
				}
			} else {
				if(t==1){
					output("%class_"+attr.type ,1);
				} else if (t==2) {
					output("%class_"+attr.type+" "+attr.id ,1);
				}
			}
			if (t==3) {
				output("%class_"+attr.type+" %"+attr.id+ " = %class_"+attr.type+" ",1);
				output("%"+globalid+" = getelementptr inbounds %object_"+c.name+"* %ret_obj, i32 0, i32 "+attr.pos,indent);
				output("store %class_"+attr.type+" %"+globalid+", %class_"+attr.type+"* %"+attr.id,indent);
				globalid++;
			}
		}
	}

	private void genClassMethods(CoolChecker.CheckerClass c, int t){
		Iterator mi = c.methods.values().iterator();
		while(mi.hasNext()){
			CoolChecker.CheckerMethod m = (CoolChecker.CheckerMethod)(mi.next());
			if(mi.hasNext()){
				if(t==1){
					String argv = getMethodArguments(m,c,2);
					output("\t%class_"+m.type+" ("+argv+")*,");
				} else if (t==2) {
					String argv = getMethodArguments(m,c,2);
					output("\t%class_"+m.type+" ("+argv+")* @"+c.name+"_"+m.name+",");
				} else if (t==3) {
					String argv = getMethodArguments(m,c,1);
					CoolNode feature1 = m.root.b;
					if(feature1.num!=1){	// native
						output("define %class_"+m.type+" @"+c.name+"_"+m.name+"("+argv+"){");
						genMethodImpl(m,c);
						output("}");
						globalid=1;
						lastreg=-1;
					} else {
						/*
						output("define %class_"+m.type+" @"+c.name+"_"+m.name+"("+argv+"){");
						output("%1 = %class_"+m.type+" null",1);
						output("ret %class_"+m.type+" %1",1);
						output("}");
						*/
					}
				}
			} else {
				if(t==1){
					String argv = getMethodArguments(m,c,2);
					output("\t%class_"+m.type+" ("+argv+")*");
				} else if (t==2) {
					String argv = getMethodArguments(m,c,2);
					output("\t%class_"+m.type+" ("+argv+")* @"+c.name+"_"+m.name);
				} else if (t==3) {
					CoolNode feature1 = m.root.b;
					String argv = getMethodArguments(m,c,1);
					if(feature1.num!=1){	// native
						output("define %class_"+m.type+" @"+c.name+"_"+m.name+"("+argv+"){");
						genMethodImpl(m,c);
						output("}");
						globalid=1;
					} else {
						/*
						output("define %class_"+m.type+" @"+c.name+"_"+m.name+"("+argv+"){");
						output("%1 = %class_"+m.type+" null",1);
						output("ret %class_"+m.type+" %1",1);
						output("}");
						*/
					}
				}
			}
		}
	}

	private String getMethodArguments(CoolChecker.CheckerMethod m,CoolChecker.CheckerClass c,int t){
		String arguments = new String();
		for(int i=0;i<m.arguments.size();i++){
			if(m.arguments.get(i).reg==-1){
				m.arguments.get(i).reg = globalid;
				//globalid++;
			}
			if(t==1)
				arguments += "%class_"+m.arguments.get(i).type + " %" + m.arguments.get(i).id ;
			else
				arguments += "%class_"+m.arguments.get(i).type;
			if(i<m.arguments.size())
				arguments += ", ";
		}
		if(t==1)
			arguments += "%object_"+c.name+"* %this";
		else
			arguments += "%object_"+c.name+"* ";
		return arguments;
	}

	private void genClassAttributes(CoolChecker.CheckerClass c, int t){
		Iterator ai = c.attributes.values().iterator();
		while(ai.hasNext()){
			CoolChecker.CheckerAttribute a = (CoolChecker.CheckerAttribute)(ai.next());
			a.pos=varpos;
			varpos++;
			if(ai.hasNext()){
				if(t==1){
					output("%class_"+a.type+" , ; "+a.id+":"+a.type,1);
				} else if (t==2) {
					output("%class_"+a.type+" @"+a.id+" ,",1);
				}
			} else {
				if(t==1){
					output("%class_"+a.type+" ;"+a.id+":"+a.type,1);
				} else if (t==2) {
					output("%class_"+a.type+" @"+a.id+" ",1);
				}
			}
			if (t==3) {
				output("%class_"+a.type+" %"+a.id+ " = %class_"+a.type+" ",1);
				output("%"+globalid+" = getelementptr inbounds %object_"+c.name+"* %ret_obj, i32 0, i32 "+a.pos,indent);
				output("store %class_"+a.type+" %"+globalid+", %class_"+a.type+"* %"+a.id,indent);
				globalid++;
			}
		}

	}

	private void genMethodImpl(CoolChecker.CheckerMethod m, CoolChecker.CheckerClass c){
		genCode(m.root,c, m);
		output("ret %class_"+m.type+" %"+lastreg,indent);
	}

	private void genCode(CoolNode n, CoolChecker.CheckerClass c, CoolChecker.CheckerMethod m){
		String value;
		switch(n.nodeType){

			case 27:	// primary
				if(n.num==13){	//this
					//n.reg = globalid;
					//globalid++;
					genChildren(n,c,m);
				}
				else if(n.num==12){ //boolean
					// Use String of "True" and "False" instead
					// get new register id
					n.reg = globalid;
					globalid++;
					value = (String)((CoolPrimary)n).sa.value;
					if(value.equals("true")){
						// true = 1
						output("%"+n.reg+" = i32 1;",indent);
					} else {
						// false = 0
						output("%"+n.reg+" = i32 0;",indent);
					}
					lastreg=n.reg;
				}
				else if(n.num==11){//string
					n.reg = globalid;
					globalid++;
					value = (String)((CoolPrimary)n).sa.value;
					output("@.str"+n.reg+" = private constant ["+(value.length()-2)+" x i8] c"+value+", align 1;",indent);
					lastreg=n.reg;
				}
				else if(n.num==10){	//integer
					n.reg = globalid;
					globalid++;
					//how to get the integer name
					value = (String)((CoolPrimary)n).sa.value;
					output("%"+n.reg+" = i32 "+value+";",indent);
					lastreg=n.reg;
				}
				else if(n.num==9){	// ID

					value = (String)((CoolPrimary)n).sa.value;
					if(m.hasArgument(value)){
						CoolChecker.CheckerAttribute a = m.getArgument(value);
						output("%"+globalid+" = %class_"+a.type+" %"+a.id,1);
						a.reg=globalid;
						n.reg=globalid;
						globalid++;
					} else if(c.variableStack.containsKey(value)){
						// have already defined this variable
						//Stack s = c.variableStack.get(id);
					} else if(c.arguments.containsKey(value)){
						// if is part of the arguments
						CoolChecker.CheckerAttribute attr = c.arguments.get(value);
						if(attr.reg==-1){
							// not yet get a register for this argument
							output("%"+globalid+" = getelementptr inbounds %object_"+c.name+"* %this, i32 0, i32 "+attr.pos+" ; "+attr.id+":"+attr.type,indent);
							globalid++;
							output("%"+globalid+" = load %class_"+attr.type+"* %"+(globalid-1),indent);
							attr.reg=globalid;
							attr.addr_reg=globalid-1;
							lastreg=globalid;
							globalid++;

						} 
						n.reg = attr.reg;
					} else if(c.attributes.containsKey(value)){
						// if it's one of the class attributes
						CoolChecker.CheckerAttribute attr = c.attributes.get(value);
						if(attr.reg==-1){
							// not yet get a register for this argument
							output("%"+globalid+" = getelementptr inbounds %object_"+c.name+"* %this, i32 0, i32 "+attr.pos+" ; "+attr.id+":"+attr.type,indent);
							globalid++;
							output("%"+globalid+" = load %class_"+attr.type+"* %"+(globalid-1),indent);
							attr.reg=globalid;
							lastreg=globalid;
							globalid++;
						} 
						n.reg = attr.reg;
					} else {
						// the ancestors attribute
					}
				}
				else if(n.num==8){	// Null
					n.reg = globalid;
					globalid++;
					value = (String)((CoolPrimary)n).sa.value;
					output("@.str"+n.reg+" = private unnamed_addr constant ["+(value.length()-2)+" x i8] c"+value+", align 1",indent);
					lastreg=n.reg;
				}
				else if(n.num==7){	// ()
				}
				else if(n.num==6){	// (expr)
					genChildren(n,c,m);
					n.reg = n.a.reg;
					lastreg=n.reg;
				}
				else if(n.num==5){	// {}
				}
				else if(n.num==4){	// {expr}
					c.level++;
					genChildren(n,c,m);
					c.level--;
					n.reg = n.a.reg;
					lastreg=n.reg;
				}
				else if(n.num==3){	// super.id()
					genChildren(n,c,m);
					value = (String)((CoolPrimary)n).sa.value;
					CoolChecker.CheckerMethod method = c.parent.getMethod(value);
					if(method != null){
						if(dotreg==-2){
							output("%"+globalid+" = call %class_"+method.type+" @"+c.name+"_"+method.name+"("+getActuals(n.a,1)+", %object_"+c.name+" %this)",1);
						} else {
							output("%"+globalid+" = call %class_"+method.type+" @"+c.name+"_"+method.name+"("+getActuals(n.a,1)+", %object_"+dotString+" %"+dotreg+")",1);
							dotreg=-2;
							dotString="";
						}
						n.reg=globalid;
						lastreg = n.reg;
						globalid++;
					} else {
						output("; CANNOT FIND METHOD! "+value,1);
					}
				}
				else if(n.num==2){	// id()
					genChildren(n,c,m);
					value = (String)((CoolPrimary)n).sa.value;
					CoolChecker.CheckerMethod method = c.getMethod(value);
					if(method != null){
						if(dotreg==-2){
							output("%"+globalid+" = call %class_"+method.type+" @"+c.name+"_"+method.name+"("+getActuals(n.a,1)+", %object_"+c.name+" %this)",1);
						} else {
							output("%"+globalid+" = call %class_"+method.type+" @"+c.name+"_"+method.name+"("+getActuals(n.a,1)+", %object_"+dotString+" %"+dotreg+")",1);
							dotreg=-2;
							dotString="";
						}
						n.reg=globalid;
						lastreg = n.reg;
						globalid++;
					} else {
						output("; CANNOT FIND METHOD! "+value,1);
					}
					// %2 = call something
				}
				else if(n.num==1){	// new class()
					genChildren(n,c,m);
					value = (String)((CoolPrimary)n).sb.value;
					//TODO: call constructor
					output("%"+globalid+" = call %object_"+value+" @"+value+"_constructor("+getActuals(n.a,1)+")",1);
					globalid++;
					output("%"+globalid+" = load %object_"+value+"* %"+(globalid-1),indent);
					globalid++;

					n.reg=globalid;
					lastreg = n.reg;

				}

				break;
				/*
				   case 5:         // block2
				   genCode(n,c,m);
				// Define local variable. Maintain variableStack.
				String id = (String)((CoolBlock2)n).sb.value;
				String type = (String)((CoolBlock2)n).sd.value;
				if(c.variableStack.containsKey(id)){
				// have already defined this variable
				Stack s = c.variableStack.get(id);
				while(s.size()>=c.level){
				s.pop();
				}
				s.push(type);
				} else {
				Stack s = new Stack();
				s.push(type);
				c.variableStack.put(id,s);
				}
				break;
				 */
			case 6:		// case
				c.level++;
				genChildren(n,c,m);
				c.level--;
				break;

			case 15:	//control
				if(n.num==3){
					// pass it on
					genChildren(n,c,m);
					n.reg = n.a.reg;
				} else if (n.num==1) {
					// if (expr1) expr2 else expr3
					int endlabel = globalendlabel;
					globalendlabel++;

					genCode(n.a,c,m);
					output("%cmp = icmp eq i32 %"+n.a.reg+", 0",1);
					output("br i1 %cmp, label %l"+globallabel+", label %l"+(globallabel+1),1);

					output("l"+globallabel+":",0);
					globallabel++;

					genCode(n.b,c,m);

					output("br label %e"+endlabel,1);

					output("l"+globallabel+":",0);
					globallabel++;

					genCode(n.c,c,m);

					output("e"+endlabel+":",0);

				} else if (n.num==2) {

					int startlabel=globallabel;
					int endlabel;
					output("l"+globallabel+":",0);
					globallabel++;

					genCode(n.a,c,m);
					output("%cmp = icmp eq i32 %"+n.a.reg+", 0",1);
					output("br i1 %cmp, label %l"+globallabel+", label %e"+(globallabel+1),1);

					output("l"+globallabel+":",0);
					globallabel++;
					endlabel=globallabel;
					globallabel++;

					genCode(n.b,c,m);

					output("br label %l"+startlabel,1);

					output("e"+endlabel+":",0);
					globallabel++;


				}
				break;
			case 16:	//dot
				if(n.num==1){
					// pass it on
					genChildren(n,c,m);
					n.reg = n.a.reg;
				} else {
					genCode(n.a,c,m);
					dotreg=n.a.reg;
					dotString=n.a.returnType;
//					output("DOTREG: "+dotreg,2);
					genCode(n.b,c,m);
					n.reg=n.b.reg;
				}
				break;
			case 17:	//equiv
				genChildren(n,c,m);
				if(n.num==1){
					// pass it on
					n.reg = n.a.reg;
				} else {
					n.reg=globalid;
					output("%"+globalid+" = icmp eq i32 %"+n.a.reg+", i32 %"+n.b.reg,1);
					globalid++;
					lastreg=n.reg;
				}
				break;
			case 18:	//expr
				genChildren(n,c,m);
				if(n.num==2){
					n.reg = n.a.reg;
					lastreg=n.reg;
				} else {
					// ID = expr;
					value = (String)((CoolExpr)n).sa.value;
//					output(";EXPR! ID:"+value,1);
					/*
					CoolChecker.CheckerAttribute attr = c.attributes.get(value);
					if(attr.reg==-1){
						// not yet get a register for this argument
						output("%"+globalid+" = getelementptr inbounds %object_"+c.name+"* %this, i32 0, i32 "+attr.pos+" ; "+attr.id+":"+attr.type,indent);
						globalid++;
						output("%"+globalid+" = load %class_"+attr.type+"* %"+(globalid-1),indent);
						attr.reg=globalid;
						attr.addr_reg=globalid-1;
						lastreg=globalid;
						globalid++;
					} 

					output("store %class_"+attr.type+" %"+n.a.reg+", %class_"+attr.type+"* %"+attr.addr_reg,indent);
					*/
					n.reg = n.a.reg;
					lastreg=n.reg;
				}
				break;
			case 19:	//feature
				genChildren(n,c,m);
				break;
			case 20:	//feature1
				genChildren(n,c,m);
				break;
			case 21:	//feature2
				genChildren(n,c,m);
				break;
			case 22:	//formals
				genChildren(n,c,m);
				break;
			case 23:	//formals1
				genChildren(n,c,m);
				if(n.num==1){

				} else if (n.num==2) {
					// ID: TYPE , formals1	
				}

				break;
			case 24:	//match
				genChildren(n,c,m);
				if(n.num==1){
					// pass it on
					n.reg = n.a.reg;
				}
				break;
			case 28:
				genChildren(n,c,m);
				if(n.num==1)	//prod
				{
					n.reg = globalid;
					globalid++;
					output("%"+n.reg+" = mul %"+n.a.reg+", %"+n.b.reg+";",indent);
					lastreg=n.reg;
				}
				else if(n.num==2)
				{
					n.reg = globalid;
					globalid++;
					output("%"+n.reg+" = div %"+n.a.reg+", %"+n.b.reg+";",indent);
					lastreg=n.reg;
				}
				else if(n.num==3)
				{
					n.reg = n.a.reg;	
				}
				break;
			case 25:	//neg
				genChildren(n,c,m);
				if(n.num==1)
				{
					n.reg = globalid;
					globalid++;
					//TODO minus
					output("%"+n.reg+" = sub i32 0, %"+n.a.reg+";",indent);
					lastreg=n.reg;
				}
				else if(n.num==2)
				{
					n.reg = globalid;
					globalid++;
					//TODO output neg
				}
				else if(n.num==3)
				{
					n.reg = n.a.reg;
				}
				break;
			case 30:	//relation
				genChildren(n,c,m);
				if(n.num==3){
					// pass it on
					n.reg = n.a.reg;
				}
				break;
			case 31:	// sum
				genChildren(n,c,m);
				if(n.num==1)	// add
				{
					n.reg = globalid;
					globalid++;
					output("%"+n.reg+" = add %"+n.a.reg+", %"+n.b.reg+";",indent);
					lastreg=n.reg;
				}
				else if(n.num==2) //minus
				{
					n.reg = globalid;
					globalid++;
					output("%"+n.reg+" = sub %"+n.a.reg+", %"+n.b.reg+";",indent);
					lastreg=n.reg;
				}
				else if(n.num==3)//prod
				{
					n.reg = n.a.reg;
				}
				break;
			default:
				genChildren(n,c,m);
		}
	}

	private void genChildren(CoolNode n, CoolChecker.CheckerClass c, CoolChecker.CheckerMethod m){
		if(n.a!=null)
			genCode(n.a,c,m);
		if(n.b!=null)
			genCode(n.b,c,m);
		if(n.c!=null)
			genCode(n.c,c,m);
	}

	private void output(String str){
		switch(this.outputType){
			case 0:
				System.out.println(str);
				break;
			case 1:
				try{
					out.write(str);
					out.newLine();
					//out.close();
				}catch (Exception e){//Catch exception if any
					System.err.println("File Writing Error: " + e.getMessage());
				}
				break;
		}
	}

	private void output(String str, int k){
		switch(this.outputType){
			case 0:
				for(int i=0; i<k;i++){
					System.out.printf("\t");
				}
				System.out.println(str);
				break;
			case 1:
				try{
					for(int i=0; i<k;i++){
						out.write("\t");
					}
					out.write(str);
					out.newLine();
					//out.close();
				}catch (Exception e){//Catch exception if any
					System.err.println("File Writing Error: " + e.getMessage());
				}
				break;
		}
	}

	private String getActuals(CoolNode n, int type){
		String ret = new String();
		if(n.num==1){
			ret="";
		} else{
			CoolNode tmp=n.a;
			while(tmp.num!=1){
				ret+="%class_"+tmp.a.returnType+" %"+tmp.a.reg+", ";
				tmp=tmp.b;
			}
			ret+="%class_"+tmp.a.returnType+" %"+tmp.a.reg;
		}
		return ret;
	}
}
